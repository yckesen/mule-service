oc config use-context minishift

oc login -u system:admin

oc new-project mule-poc --description="Mule Poc Project" --display-name="Mule Po"

# 
# Set UID to expected value
# The mule-base image currently doesn't support random uids
# 
#oc annotate ns mule-test openshift.io/sa.scc.uid-range=1000/1 --overwrite

#
# Create docker-registry secret to access private images
#
oc create secret -n mule-test docker-registry gitlab-registry \
  --docker-server=registry.gitlab.com \
  --docker-username=$GITLAB_REGISTRY_USERNAME \
  --docker-password=$GITLAB_REGISTRY_PASSWORD

#
# Deploy service
#
oc apply -f deployment.yaml
oc apply -f service.yaml
