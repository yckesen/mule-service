#FROM registry.gitlab.com/bherrmann/oc_poc-base-mule/mule-base-image/mule-base-image:master
FROM oc_poc-base-mule:latest

MAINTAINER heb "info@heb.ch"

COPY target/poc_s-api-mule-service-1.0.0-SNAPSHOT-mule-application.jar /opt/mule/mule-enterprise-standalone-4.2.1-hf1/apps/poc_s-api-mule-service-1.0.0-SNAPSHOT-mule-application.jar

EXPOSE 8083

CMD ["/opt/mule/mule-enterprise-standalone-4.2.1-hf1/bin/mule"]

