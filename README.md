# oc_poc_s-layer-mule

Project for docker container provides s-layer mule services.

provides 2 services:
http://localhost:8083/status
http://localhost:8083/customers


Note for docker build:
$ docker image build -t oc_poc_s-api-mule .


docker container run -p 8080:8083 -d oc_poc_s-api-mule


before docker you have to build mule app with:
$ mvn clean package
